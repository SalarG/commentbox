import React from 'react';
import {connect} from 'react-redux';
import {fetchComments, saveComment} from '../actions/index';
import requireAuth from "./requireAuth";

class CommentBox extends React.Component {
    state = {comment: ''}

    componentDidMount() {
        this.shouldNavigateAway()
    }

    componentDidUpdate() {
        this.shouldNavigateAway()
    }

    shouldNavigateAway = () => {
        if (!this.props.auth) {
            this.props.history.push('/')
        }
    }
    handleChange = event => {
        this.setState({comment: event.target.value});
    }

    handleSubmit = event => {
        event.preventDefault();
        this.props.saveComment(this.state.comment);
        this.setState({comment: ''});
    }

    render() {
        return (
            <div>
                <form onSubmit={this.handleSubmit}>
                    <h4>Add a Comment</h4>
                    <textarea
                        onChange={this.handleChange}
                        value={this.state.comment}

                    />
                    <div>
                        <button>Submit Comment</button>
                    </div>
                </form>
                <button className="fetch-comments" onClick={this.props.fetchComments}>Fetch Comments</button>
            </div>
        );
    }
}


const mapDispatchToProps = {
    fetchComments, saveComment
}

export default connect(null, mapDispatchToProps)(requireAuth(CommentBox));